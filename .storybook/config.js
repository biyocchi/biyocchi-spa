import { configure, storiesOf } from '@storybook/react';
import '@storybook/addon-console';
import React from 'react';

const req = require.context('./stories', true, /.jsx$/);
const reqStrap = require.context('./stories/reactstrap', true, /.js$/);
const reqStyleguide = require.context('./stories/styleguide', true, /.jsx$/);


function loadStories() {
  require('../src/assets/css/main.scss');
  req.keys().forEach(filename => req(filename));
}


function loadReactstrapExamples() {
  require('../src/assets/css/main.scss');
  reqStrap.keys().forEach(filename => reqStrap(filename));
}

function loadStyleguide() {
  require('../src/assets/css/main.scss');
  {/* Typography */}
  {/* Colors */}
  {/* Forms */}
  {/* Buttons */}
  {/* Alerts */}
  {/* Labels */}
  {/* Pagination */}
  {/* Breadcrumbs */}
  {/* Panel */}
  {/* Modal */}
  {/* Popover */}
  {/* Tooltip */}
  {/* Navs */}
  {/* List Group */}
  {/* Media List */}
  {/* Progress */}
  {/* Tables */}
  {/* Icons */}
  {/* Glyphicons */}
  {/* Page-specific layouts */}
  reqStyleguide.keys().forEach(filename => reqStyleguide(filename));
}

configure(loadStories, module);
configure(loadReactstrapExamples, module);
configure(loadStyleguide, module);
