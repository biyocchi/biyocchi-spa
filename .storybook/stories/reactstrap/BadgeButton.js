import React from 'react';
import { storiesOf } from '@storybook/react';
import { Badge, Button } from 'reactstrap';

export default class BadgeButton extends React.Component {
  render() {
    return (
      <div>
        <Button color="primary" outline>
          Notifications <Badge color="secondary">4</Badge>
        </Button>
      </div>
    );
  }
}

storiesOf('Reactstrap', module).add('BadgeButton', () => <BadgeButton />);
