import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button, ButtonGroup } from 'reactstrap';

export default class ButtonGroupExample extends React.Component {
  render() {
    return (
      <ButtonGroup>
        <Button>Left</Button>
        <Button>Middle</Button>
        <Button>Right</Button>
      </ButtonGroup>
    );
  }
}

storiesOf('Reactstrap', module).add('ButtonGroup', () => <ButtonGroupExample />);
