import React from 'react';
import { storiesOf } from '@storybook/react';
import { Form, FormGroup, Label, Input } from 'reactstrap';

export default class InlineCheckboxes extends React.Component {
  render() {
    return (
      <Form>
        <FormGroup check inline>
          <Label check>
            <Input type="checkbox" /> Some input
          </Label>
        </FormGroup>
        <FormGroup check inline>
          <Label check>
             <Input type="checkbox" /> Some other input
          </Label>
        </FormGroup>
      </Form>
    );
  }
}

storiesOf('Reactstrap', module).add('InlineCheckboxes', () => <InlineCheckboxes />);
