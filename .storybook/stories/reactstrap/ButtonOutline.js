import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from 'reactstrap';

export default class ButtonOutline extends React.Component {
  render() {
    return (
      <div>
        <Button outline color="primary">primary</Button>{' '}
        <Button outline color="secondary">secondary</Button>{' '}
        <Button outline color="success">success</Button>{' '}
        <Button outline color="info">info</Button>{' '}
        <Button outline color="warning">warning</Button>{' '}
        <Button outline color="danger">danger</Button>
      </div>
    );
  }
}

storiesOf('Reactstrap', module).add('ButtonOutline', () => <ButtonOutline />);
