import React from 'react';
import { storiesOf } from '@storybook/react';
import { Alert } from 'reactstrap';

const AlertContent = (props) => {
  return (
    <div>
      <Alert color="success">
        <h4 className="alert-heading">Well done!</h4>
        <p>
          Aww yeah, you successfully read this important alert message. This AlertContent text is going
          to run a bit longer so that you can see how spacing within an alert works with this kind
          of content.
        </p>
        <hr />
        <p className="mb-0">
          Whenever you need to, be sure to use margin utilities to keep things nice and tidy.
        </p>
      </Alert>
    </div>
  );
};

storiesOf('Reactstrap', module).add('AlertContent', () => <AlertContent />);
