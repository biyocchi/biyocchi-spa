/* eslint react/no-multi-comp: 0, react/prop-types: 0 */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { UncontrolledButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';

export default class ButtonDropdownUncontrolled extends React.Component {
	render() {
		return (
			<UncontrolledButtonDropdown>
				<DropdownToggle caret>
					Dropdown
      			</DropdownToggle>
				<DropdownMenu>
					<DropdownItem header>Header</DropdownItem>
					<DropdownItem disabled>Action</DropdownItem>
					<DropdownItem>Another Action</DropdownItem>
					<DropdownItem divider />
					<DropdownItem>Another Action</DropdownItem>
				</DropdownMenu>
			</UncontrolledButtonDropdown>
		);
	}
}

storiesOf('Reactstrap', module).add('ButtonDropdownUncontrolled', () => <ButtonDropdownUncontrolled />);
