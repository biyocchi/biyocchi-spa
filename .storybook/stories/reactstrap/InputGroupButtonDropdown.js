import React from 'react';
import { storiesOf } from '@storybook/react';
import PropTypes from 'prop-types';
import { InputGroupButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

const propTypes = {
  addonType: PropTypes.oneOf(['prepend', 'append']).isRequired,
};

class InputGroupButtonDropdownExample extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    const { addonType } = this.props;

    return (
      <InputGroupButtonDropdown addonType={addonType} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret>
          Button Dropdown
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem header>Header</DropdownItem>
          <DropdownItem disabled>Action</DropdownItem>
          <DropdownItem>Another Action</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>Another Action</DropdownItem>
        </DropdownMenu>
      </InputGroupButtonDropdown>
    );
  }
}

InputGroupButtonDropdownExample.propTypes = propTypes;

storiesOf('Reactstrap', module).add('InputGroupButtonDropdown', () => <InputGroupButtonDropdownExample />);
