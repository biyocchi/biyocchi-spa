import React from 'react';
import { storiesOf } from '@storybook/react';
import { UncontrolledAlert } from 'reactstrap';

function AlertUncontrolledDismiss() {
  return (
    <UncontrolledAlert color="info">
      I am an alert and I can be dismissed!
    </UncontrolledAlert>
  );
}

storiesOf('Reactstrap', module).add('AlertUncontrolledDismiss', () => <AlertUncontrolledDismiss />);
