import React from 'react';
import { storiesOf } from '@storybook/react';
import { Spinner } from 'reactstrap';

export default class SpinnerExample extends React.Component {
  render() {
    return (
      <div>
        <Spinner color="primary" />
        <Spinner color="secondary" />
        <Spinner color="success" />
        <Spinner color="danger" />
        <Spinner color="warning" />
        <Spinner color="info" />
        <Spinner color="light" />
        <Spinner color="dark" />
      </div>
    );
  }
}

storiesOf('Reactstrap', module).add('SpinnerExample', () => <SpinnerExample />);
