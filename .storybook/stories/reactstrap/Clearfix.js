import React from 'react';
import { storiesOf } from '@storybook/react';

const Clearfix = () => {
  return (
    <div className="bg-info clearfix" style={{ padding: '.5rem' }}>
      <button className="btn btn-secondary float-left">Clearfix Button floated left</button>
      <button className="btn btn-danger float-right">Clearfix Button floated right</button>
    </div>
  );
};

storiesOf('Reactstrap', module).add('Clearfix', () => <Clearfix />);
