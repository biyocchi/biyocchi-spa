import React from 'react';
import { storiesOf } from '@storybook/react';
import { ListGroup, ListGroupItem } from 'reactstrap';

export default class ListGroupExample extends React.Component {
  render() {
    return (
      <ListGroup>
        <ListGroupItem>Cras justo odio</ListGroupItem>
        <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
        <ListGroupItem>Morbi leo risus</ListGroupItem>
        <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
        <ListGroupItem>Vestibulum at eros</ListGroupItem>
      </ListGroup>
    );
  }
}

storiesOf('Reactstrap', module).add('ListGroupExample', () => <ListGroupExample />);
