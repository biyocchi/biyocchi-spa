import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { Card, CardTitle, CardText, Row, Col } from 'reactstrap';

const Typography = () => (
  <Fragment>
    <div className="container">
      <div>
        <h1 className="display-4">Typography</h1>
        <hr />
      </div>

      <h6>Font Family</h6>
      <p className="small">
        -apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Open Sans","Helvetica Neue",sans-serif !default;
      </p>

      <br/>
    </div>
  </Fragment>
);

storiesOf('Style guide', module).add('Typography', () => <Typography />);
