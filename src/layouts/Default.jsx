import React, { Suspense } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const Default = ({ children }) => <Suspense fallback="loading">{children}</Suspense>;

Default.propTypes = propTypes;
export default Default;
