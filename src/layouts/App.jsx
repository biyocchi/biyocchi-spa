import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { Spinner } from 'reactstrap';

import useAuth from '../hooks/useAuth';

const propTypes = {
  children: PropTypes.node,
};

const App = ({ children }) => {
  const { authorized, loading } = useAuth();

  if (loading) {
    return <Spinner color="dark" />;
  }

  if (!authorized) {
    return <Redirect to="/login" />;
  }

  return <Suspense fallback="loading">{children}</Suspense>;
};

App.propTypes = propTypes;
export default App;
