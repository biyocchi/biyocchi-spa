export function classNames() {
  let prepared = [];

  // arguments.forEach not available
  for (var i = arguments.length - 1; i >= 0; i--) {
    let arg = arguments[i];

    switch (typeof arg) {
      case 'string':
        // When single string, just push into prepared array
        prepared.push(arg);
        break;
      case 'object':
        // Could be array or single object, but we expect obj.
        if (Array.isArray(arg)) {
          throw new Error();
        }

        // Select it if value = true
        if (Object.values(arg)[0]) {
          prepared.push(Object.keys(arg)[0]);
        }
        break;
      case 'function':
        let item = arg();

        prepared.push(item);
        break;
      default:
        prepared.push(undefined);
    }
  }

  // Finally select all non-undefined classes
  return prepared.filter(obj => obj !== undefined).join(' ');
}
