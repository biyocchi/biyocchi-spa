import axios from 'axios';

// Raw perform method
export const perform = async (url, method, data = undefined) => {
  return axios({
    url: process.env.REACT_APP_API_URL.concat(url),
    method,
    headers: {
      Accept: 'application/json; charset=utf-8',
      'Content-Type': 'application/json',
    },
    withCredentials: true,
    data,
  });
};

// Get
export const get = async (url, data = undefined) => {
  return axios({
    url: process.env.REACT_APP_API_URL.concat(url).concat('.json'),
    method: 'get',
    headers: {
      Accept: 'application/json; charset=utf-8',
      'Content-Type': 'application/json',
    },
    withCredentials: true,
    data,
  });
};

// Post
export const post = async (url, data = undefined) => {
  return axios({
    url: process.env.REACT_APP_API_URL.concat(url).concat('.json'),
    method: 'post',
    headers: {
      Accept: 'application/json; charset=utf-8',
      'Content-Type': 'application/json',
    },
    withCredentials: true,
    data,
  });
};

// Delete
export const del = async (url, data = undefined) => {
  return axios({
    url: process.env.REACT_APP_API_URL.concat(url).concat('.json'),
    method: 'delete',
    headers: {
      Accept: 'application/json; charset=utf-8',
      'Content-Type': 'application/json',
    },
    withCredentials: true,
    data,
  });
};

// Patch
export const patch = async (url, data = undefined) => {
  return axios({
    url: process.env.REACT_APP_API_URL.concat(url).concat('.json'),
    method: 'patch',
    headers: {
      Accept: 'application/json; charset=utf-8',
      'Content-Type': 'application/json',
    },
    withCredentials: true,
    data,
  });
};
