// This HOC is NOT a page. this simply wraps & routes the rest of the app's HOCs
import React from 'react';
import { Spinner } from 'reactstrap';

import useAuth from './hooks/useAuth';

// Authorized
import AppRoutes from './app';
// Unauthorized
import PageRoutes from './pages';

// App is all authorized in one form or another.

const Routes = () => {
  const { authorized, loading, user } = useAuth();

  // IF NOT AUTHORIZED
  if (!authorized) {
    return <PageRoutes />;
  }

  // IF AUTHORIZED BUT CURRENT USER DATA NOT LOADED
  if (loading && !user) {
    return <Spinner color="dark" />;
  }

  // IF AUTHORIZED AND CURRENT USER INFORMATION LOADED
  return <AppRoutes />;
};

export default Routes;
