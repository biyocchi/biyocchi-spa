import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
// import classNames from 'classnames';

const Input = ({ name, label, type, /* validationMessage, */ ...other }) => (
  <Field name={name}>
    {({ field, form }) => (
      <div className="form-group">
        {label && (
          <label className="control-label" htmlFor={name}>
            {label}
          </label>
        )}
        <input className="form-control" name={name} id={name} type={type} {...field} {...other} />
        {/*
          {_.get(form.errors, name) && _.get(form.touched, name) && validationMessage && (
            <span className="help-block validation-message">{validationMessage}</span>
          )}
        */}
      </div>
    )}
  </Field>
);

Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  type: PropTypes.string,
  // validationMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};
Input.defaultProps = {
  type: 'text',
};
export default Input;
