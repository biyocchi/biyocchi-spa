import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import { Row, Col, Media } from 'reactstrap';

export default Object.assign(
  ({ dropzoneConfig, labels, multi, fileList, dropzoneStyle, thumbnailStyle, onDrop }) => {
    const dropzoneSize = fileList.length % 2 === 0 ? 12 : 6;

    const defaultConfig = {
      minsize: 1,
      maxSize: 10485760, // 10mb
      accept: 'image/*',
      multiple: !!multi,
      onDrop,
    };

    const { getRootProps, getInputProps, isDragActive, isDragReject } = useDropzone(
      dropzoneConfig || defaultConfig,
    );

    return (
      <Row>
        {fileList.map(file => (
          <Col key={file.name} xs={6}>
            <Media object src={file.preview} style={thumbnailStyle} />
            <p
              className="text-muted text-truncate"
              style={{ fontFamily: 'Helvetica', fontSize: '0.6em' }}
            >
              {file.path}
            </p>
          </Col>
        ))}
        <Col xs={dropzoneSize}>
          <div {...getRootProps()} style={dropzoneStyle}>
            <input {...getInputProps()} />
            <span style={{ fontSize: '0.8em' }}>
              {!isDragActive && labels.waiting}
              {!isDragActive && isDragReject && labels.rejected}
            </span>
          </div>
        </Col>
      </Row>
    );
  },
  {
    propTypes: {
      dropzoneConfig: PropTypes.object,
      thumbnailStyle: PropTypes.object,
      dropzoneStyle: PropTypes.object,
      labels: PropTypes.shape({
        waiting: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
        rejected: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
      }),
      multi: PropTypes.bool,
      fileList: PropTypes.array.isRequired,
      onDrop: PropTypes.func.isRequired,
    },
  },
  {
    defaultProps: {
      multi: true,
      labels: {
        waiting: 'Click or drag files here to upload.',
        rejected: 'Cannot accept these files.',
      },
      dropzoneStyle: {
        height: '100%',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 20,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out',
      },
      thumbnailStyle: {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        padding: 4,
        boxSizing: 'border-box',
        width: '100%',
      },
    },
  },
);
