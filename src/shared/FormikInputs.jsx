import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { FormGroup, Label, Input, FormText, Col, FormFeedback, Button } from 'reactstrap';
// import classNames from 'classnames';
import { useTranslation } from 'react-i18next';

/* RADIO SET */
export const RadioFields = Object.assign(
  ({ fieldName, value, options }) => {
    const RadioButton = ({ id, label, field: { name, value, onChange } }) => (
      <FormGroup check>
        <Label check>
          <Input
            name={name} // For Formik
            id={id}
            type="radio"
            value={id}
            checked={id === value}
            onChange={onChange}
          />
          {label}
        </Label>
      </FormGroup>
    );

    return (
      <React.Fragment>
        {options.map(option => (
          <Field
            key={`${fieldName}-${option.value}`}
            component={RadioButton}
            id={option.value}
            name={fieldName}
            label={option.label}
          />
        ))}
      </React.Fragment>
    );
  },
  {
    propTypes: {
      fieldName: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          label: PropTypes.string,
          value: PropTypes.string.isRequired,
        }),
      ),
    },
  },
);

/* EMAIL NO LABEL */
export const EmailPlaceholderField = Object.assign(
  ({ fieldName, placeholder /* validationMessage, */ }) => (
    <Field name={fieldName}>
      {({ field, form }) => (
        <FormGroup>
          <Label>
            <Input
              id={fieldName}
              name={fieldName}
              type="email"
              value=""
              {...field}
              placeholder={placeholder}
            />
          </Label>
        </FormGroup>
      )}
    </Field>
  ),
  {
    propTypes: {
      fieldName: PropTypes.string.isRequired,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          placeholder: PropTypes.string,
        }),
      ),
    },
  },
);

/* EMAIL w/ LABEL */
export const ModalInput = Object.assign(
  ({ type, fieldName, label, placeholder, lowerText, validationMessage }) => {
    const labelSize = 12;
    const inputSize = 12;

    return (
      <Field name={fieldName}>
        {({ field, form }) => (
          <div className="container">
            <FormGroup row style={{ alignItems: 'center' }}>
              {label && (
                <Label xs={labelSize} for={fieldName}>
                  {label}
                </Label>
              )}
              <Col xs={inputSize}>
                <Input
                  id={fieldName}
                  name={fieldName}
                  type={type}
                  {...field}
                  placeholder={placeholder || ''}
                />
              </Col>
              {validationMessage && (
                <FormFeedback valid={form.isValid}>{validationMessage}</FormFeedback>
              )}
              {lowerText && <FormText>{lowerText}</FormText>}
            </FormGroup>
          </div>
        )}
      </Field>
    );
  },
  {
    propTypes: {
      type: PropTypes.string.isRequired,
      fieldName: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
      placeholder: PropTypes.string,
      lowerText: PropTypes.string,
    },
  },
);

/* Generic input */
export const BaseInput = Object.assign(
  ({
    type,
    fieldName,
    label,
    inline,
    placeholder,
    lowerText,
    validMessage,
    showValidFeedback,
    showInvalidFeedback,
  }) => {
    const inputValid = form => !form.errors[fieldName];

    const addValidClass = form => showValidFeedback && form.touched[fieldName] && inputValid(form);
    const showValidMessage = form => addValidClass(form) && validMessage;
    const showInvalidMessage = form =>
      !inputValid(form) && form.touched[fieldName] && showInvalidFeedback;

    return (
      <Field name={fieldName}>
        {({ field, form }) => (
          <FormGroup inline={inline}>
            {label && <Label for={fieldName}>{label}</Label>}
            <Input
              id={fieldName}
              name={fieldName}
              type={type}
              {...field}
              placeholder={placeholder}
              // valid & invalid only responsible for showing feedback
              valid={addValidClass(form)}
              invalid={showInvalidMessage(form)}
            />
            {showValidMessage(form) && <FormFeedback valid>{validMessage}</FormFeedback>}
            {showInvalidMessage(form) && <FormFeedback>{form.errors[fieldName]}</FormFeedback>}
            {lowerText && <FormText>{lowerText}</FormText>}
          </FormGroup>
        )}
      </Field>
    );
  },
  {
    propTypes: {
      type: PropTypes.string.isRequired,
      inline: PropTypes.bool,
      fieldName: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
      placeholder: PropTypes.string,
      lowerText: PropTypes.string,
      showValidFeedback: PropTypes.bool,
      showInvalidFeedback: PropTypes.bool,
    },
  },
  {
    defaultProps: {
      inline: false,
      placeholder: '',
      showValidFeedback: true,
      showInvalidFeedback: true,
    },
  },
);

/* Generic input */
export const BaseSelect = Object.assign(
  ({ fieldName, label, lowerText, options }) => {
    const { t } = useTranslation('common');
    const isValid = field => field.value !== '';
    const isInvalid = (form, field) => form.touched[fieldName] && field.value === '';

    return (
      <Field name={fieldName}>
        {({ field, form }) => (
          <FormGroup>
            {label && <Label for={fieldName}>{label}</Label>}
            <Input
              id={fieldName}
              name={fieldName}
              type="select"
              {...field}
              // valid & invalid only responsible for showing feedback
              valid={isValid(field)}
              invalid={isInvalid(form, field)}
            >
              <option value="">{t('select')}</option>
              {options.map(opt => (
                <option key={opt.value} value={opt.value}>
                  {opt.label}
                </option>
              ))}
            </Input>
            {isInvalid(form, field) && <FormFeedback>{form.errors[fieldName]}</FormFeedback>}
            {lowerText && <FormText>{lowerText}</FormText>}
          </FormGroup>
        )}
      </Field>
    );
  },
  {
    propTypes: {
      fieldName: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
      lowerText: PropTypes.string,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          value: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired,
        }),
      ),
    },
  },
);

/* Generic checkbox */
export const BaseCheckbox = Object.assign(
  ({ fieldName, label, lowerText }) => {
    const isValid = field => field.value;
    const isInvalid = (form, field) => form.touched[fieldName] && !field.value;

    return (
      <Field name={fieldName}>
        {({ field, form }) => (
          <FormGroup check>
            <Label check for={fieldName}>
              <Input
                id={fieldName}
                name={fieldName}
                type="checkbox"
                {...field}
                // valid & invalid only responsible for showing feedback
                valid={isValid(field)}
                invalid={isInvalid(form, field)}
              />
              {label}
              {isInvalid(form, field) && <FormFeedback>{form.errors[fieldName]}</FormFeedback>}
            </Label>
            {lowerText && <FormText>{lowerText}</FormText>}
          </FormGroup>
        )}
      </Field>
    );
  },
  {
    propTypes: {
      fieldName: PropTypes.string.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
      lowerText: PropTypes.string,
    },
  },
  {
    defaultProps: {
      label: 'Check',
    },
  },
);

/* Submit */
export const Submit = Object.assign(
  ({ color, isValid, isSubmitting, label, disabled }) => {
    const { t } = useTranslation('common');
    const labelText = label || t('submit');

    const buttonColor = isSubmitting ? 'link' : color;
    const buttonOutline = isSubmitting;

    return (
      <Button
        disabled={!isValid || isSubmitting || disabled}
        outline={buttonOutline}
        color={buttonColor}
        type="submit"
      >
        <span className={`${isSubmitting && 'd-none'}`}>{labelText}</span>
        {isSubmitting && (
          <div className="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        )}
      </Button>
    );
  },
  {
    propTypes: {
      color: PropTypes.string,
      isValid: PropTypes.bool.isRequired,
      isSubmitting: PropTypes.bool.isRequired,
      disabled: PropTypes.bool,
      label: PropTypes.string,
    },
  },
  {
    defaultProps: {
      color: 'primary',
      disabled: false,
    },
  },
);
