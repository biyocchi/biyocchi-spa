import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { classNames } from '../utils/classNames';

const Sidebar = ({ show, mounted }) => {
  const { t } = useTranslation();
  const linkStrings = t('sidebar.links');

  return (
    <section className={classNames(!mounted && 'd-none')}>
      <span
        className={classNames(
          show ? 'sidebar-background sidebar-fade-in' : 'd-none sidebar-fade-out',
        )}
      />
      <div className={classNames('sidebar-pane', 'container', show ? 'slide-out' : 'slide-in')}>
        <ul className="list-unstyled">
          {Object.keys(linkStrings).map(key => (
            <Link key={`link${key}`} to={`${key}`}>
              <li className="ml-5 py-4">
                <span className="text-dark h5">{linkStrings[key]}</span>
              </li>
            </Link>
          ))}
        </ul>
      </div>
    </section>
  );
};

const Header = () => {
  const [showSidebar, toggleSidebar] = useState(false);
  const [mounted, doMount] = useState(false);

  const handleToggleSidebar = () => {
    // on mount, the slide-in animation will show if we do not add "display: none" to the sidebar.
    // To counteract this, we'll only set the mount state when the user first opens the sidebar pane
    if (!mounted) {
      doMount(true);
    }
    document.getElementsByTagName('body')[0].style.overflowY = `${!showSidebar ? 'hidden' : ''}`;
    toggleSidebar(!showSidebar);
  };

  return (
    <Fragment>
      {/* <header className="navbar navbar-dark bg-dark d-flex justify-content-end align-items-center"> */}
      <header
        style={{ backgroundColor: '#000' }}
        className="navbar navbar-dark d-flex justify-content-end align-items-center"
      >
        {/*
        <Link className="navbar-brand" to="/">
          <h1 className="mb-0">biyocchi</h1>
        </Link>
      */}

        <div
          id="close-animated"
          className={classNames({ 'close-icon-open': showSidebar })}
          onClick={handleToggleSidebar}
        >
          <span />
          <span />
          <span />
          <span />
        </div>
        <Sidebar show={showSidebar} mounted={mounted} />
      </header>
    </Fragment>
  );
};

export default Header;
