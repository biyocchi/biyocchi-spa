import React, { Fragment, useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';
import { useTranslation, Trans } from 'react-i18next';
import { Formik, Form } from 'formik';
import { string as yupString, object as yupObject } from 'yup';
import {
  Row,
  Col,
  Card,
  CardText,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

import Default from '../../layouts/Default';
import Header from '../../shared/Header';
import Dropzone from '../../shared/Dropzone';
import { ModalInput as Input, Submit } from '../../shared/FormikInputs';
import LandingSection from '../../containers/LandingSection';

const CallToAction = () => {
  const { t } = useTranslation('landing');

  return (
    <div className="call-to-action">
      <div className="inner">
        {/* Main text */}
        <div className="tagline">
          <p>{t('callToAction.intro')}</p>
          <h1>BIYOCCHI</h1>
          {t('callToAction.description').map((line, i) => (
            <p key={`tl${i}`}>{line}</p>
          ))}
        </div>

        {/* Button/Seal of approval thing */}
        <div className="seal">
          <div className="seal-container">
            <div className="seal-text">
              <p className="seal-title">{t('callToAction.seal.title')}</p>
              <p className="seal-subtitle">
                {t('callToAction.seal.count')}
                <span id="guestcounter">{t('callToAction.seal.counter')}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const OurService = () => {
  const { t } = useTranslation('landing');

  const gotoButton = (href, label) => (
    <a href={href} className="btn btn-dark">
      {label}
      <span />
    </a>
  );

  return (
    <LandingSection id="ourService">
      {/* Guest Diagram */}
      <div id="service-diagram-container">
        <div id="guest-diagram" className="diagram">
          <h5>{t('ourService.matchDiagram.guestTable.title')}</h5>
          <div className="diagram-list-container">
            <ul className="diagram-list">
              {t('ourService.matchDiagram.guestTable.points').map((line, i) => (
                <li key={`gService${i}`}>{line}</li>
              ))}
            </ul>
          </div>
          <div id="guest-image" className="diagram-image-container">
            {gotoButton('#price', t('ourService.matchDiagram.guestTable.goto'))}
          </div>
        </div>

        {/* Connecting point */}
        <div id="between-diagram">
          <div className="between-diagram-container">
            <div id="g-arrow" className="arrow-container">
              <span>{t('ourService.matchDiagram.betweenTables.arrowText')}</span>
            </div>
            <div id="biyocchi-matching">
              <h6 className="btn btn-dark">BIYOCCHI</h6>
              <span>{t('ourService.matchDiagram.betweenTables.matchingText')}</span>
            </div>
            <div id="h-arrow" className="arrow-container">
              <span>{t('ourService.matchDiagram.betweenTables.arrowText')}</span>
            </div>
          </div>
        </div>

        {/* Host Diagram */}
        <div id="host-diagram" className="diagram">
          <h5>{t('ourService.matchDiagram.hostTable.title')}</h5>
          <div className="diagram-list-container">
            <ul className="diagram-list">
              {t('ourService.matchDiagram.hostTable.points').map((line, i) => (
                <li key={`hService${i}`}>{line}</li>
              ))}
            </ul>
          </div>
          <div id="host-image" className="diagram-image-container">
            {gotoButton('#ranking', t('ourService.matchDiagram.hostTable.goto'))}
          </div>
        </div>
      </div>
    </LandingSection>
  );
};

const WhyBiyocchi = () => {
  const { t } = useTranslation('landing');

  const getCardText = i18nArr => {
    // Arr with object items.
    return (
      <Fragment>
        {i18nArr.map((obj, i) => {
          let out = '';

          switch (obj.tag) {
            case 'strong':
              out = <strong key={i}>{obj.text}</strong>;
              break;
            case 'br':
              out = <br key={i} />;
              break;
            default:
              out = obj.text;
              break;
          }

          // Append <br /> unless last element
          return out;
        })}
      </Fragment>
    );
  };

  return (
    <LandingSection id="whyBiyocchi">
      <Row>
        {/* one element */}
        {t('whyBiyocchi.points').map((pointArr, i) => {
          const src = require(`../../assets/images/landing/reason${i + 1}.png`);

          return (
            <Col key={i} xs={12} lg={4} className={i % 2 === 0 ? 'offset-lg-2' : ''}>
              <Card body className="text-center">
                <div className="card-img-container">
                  <img src={src} alt="" />
                </div>
                <CardText>{getCardText(pointArr)}</CardText>
              </Card>
            </Col>
          );
        })}
      </Row>
    </LandingSection>
  );
};

const Price = ({ clientModal, setClientModal }) => {
  const { t } = useTranslation('landing');

  return (
    <LandingSection id="price">
      <div className="price-box">
        <div id="flag">
          <div className="flag-text">
            <p>{t('price.flagTop')}</p>
            <p>{t('price.flagBottom')}</p>
          </div>
          <div className="flag-price">
            <p>{t('price.flagPrice')}</p>
          </div>
        </div>

        <div className="price-box-details">
          <div className="price-box-message">
            <p className="price-time">{t('price.oneTime')}</p>
            <p className="price-price">{t('price.oneTimePrice')}</p>
            <p className="price-tax">{t('price.oneTimePriceTax')}</p>
          </div>
        </div>
      </div>

      <Row className="mt-5">
        <Col xs={12} lg={6} className="offset-lg-3">
          <Button className="btn btn-block btn-dark" onClick={() => setClientModal(!clientModal)}>
            {t('price.buttonText')}
          </Button>
        </Col>
      </Row>
    </LandingSection>
  );
};

const Ranking = ({ hostModal, setHostModal }) => {
  const { t } = useTranslation('landing');
  const images = {
    green: require('../../assets/images/landing/geeen.png'),
    gold: require('../../assets/images/landing/gold.png'),
    purple: require('../../assets/images/landing/purple.png'),
    black: require('../../assets/images/landing/black.png'),
  };

  return (
    <LandingSection id="ranking">
      <h3 className="red-accent">{t('ranking.h3')}</h3>

      <Table responsive>
        <thead>
          <tr>
            {Object.keys(t('ranking.thead')).map(key => (
              <th key={key}>
                <Trans i18nKey={`landing:ranking.thead.${key}`} />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {Object.keys(images).map((color, i) => (
            <tr key={color}>
              <td>
                <img src={images[color]} alt={`${color} level`} />
                <br />
                {t(`ranking.tbody.${color}.rank`)}
              </td>
              <td>{t(`ranking.tbody.${color}.sales`)}</td>
              <td>{t(`ranking.tbody.${color}.compensation`)}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Row className="mt-5">
        <Col xs={12} lg={6} className="offset-lg-3">
          <Button className="btn btn-block btn-dark" onClick={() => setHostModal(!hostModal)}>
            {t('ranking.buttonText')}
          </Button>
        </Col>
      </Row>
    </LandingSection>
  );
};

const HostModal = ({ show, onClose }) => {
  const { t } = useTranslation('landing');
  const [files, setFiles] = useState([]);
  const [terms, showTerms] = useState(false);

  useEffect(
    () => () => {
      if (!show) {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
        setFiles([]);
      }
    },
    [files, show],
  );

  const onDrop = acceptedFiles => {
    const allFiles = [];
    files.forEach(file => allFiles.push(file));

    acceptedFiles.forEach(file =>
      allFiles.push(Object.assign(file, { preview: URL.createObjectURL(file) })),
    );

    setFiles(allFiles);
  };

  const onSubmit = (values, actions) => {
    const params = new FormData();
    actions.setSubmitting(true);

    params.append('email', values.email);
    files.forEach(file => {
      params.append(`identification_documents[]`, file);
    });

    const toastOptions = {
      autoClose: 8000,
      closeButton: false,
    };
    axios
      .post(`${process.env.REACT_APP_API_URL}/new_beautician_application`, params)
      .then(response => {
        toast.success(response.data.message, toastOptions);
        actions.setSubmitting(false);
        onClose();
      })
      .catch(error => {
        const response = error ? error.response : undefined;
        const data = response ? response.data : undefined;
        const message = data ? data.message : t('common:error');

        toast.error(message, toastOptions);
        actions.setSubmitting(false);
      });
  };

  const initialValues = {
    email: '',
  };
  const canSubmit = values => !!values.email && files.length >= 2;
  const validationSchema = yupObject().shape({
    email: yupString()
      .email()
      .required(),
  });

  const PreviewDropzone = () => (
    <section className="container">
      <div className="form-group">
        <label className="form-label" style={{ fontSize: '0.9em' }}>
          {t('hostForm.dropzone.title')}
        </label>
        <Dropzone
          labels={{
            waiting: t('hostForm.dropzone.waiting'),
            rejected: t('hostForm.dropzone.rejected'),
          }}
          onDrop={onDrop}
          fileList={files}
        />
      </div>
    </section>
  );

  return (
    <Modal isOpen={show} toggle={onClose}>
      <Formik onSubmit={onSubmit} initialValues={initialValues} validationSchema={validationSchema}>
        {({ isValid, isSubmitting, values, setFieldValue }) => (
          <Form>
            <ModalHeader toggle={onClose}>{t('hostForm.modalTitle')}</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12}>
                  <Input
                    type="email"
                    fieldName="email"
                    placeholder={t('hostForm.email.placeholder')}
                    label={t('hostForm.email.label')}
                  />
                </Col>
                <Col xs={12}>
                  <PreviewDropzone />
                </Col>
                <Col xs={12}>
                  <div className="text-center">
                    <Button color="link" className="text-muted" onClick={() => showTerms(!terms)}>
                      {t('privacyPolicy:h1')}
                    </Button>
                  </div>
                  {terms && t('privacyPolicy:applicants').map((line, i) => <p key={i}>{line}</p>)}
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Submit isValid={isValid} isSubmitting={isSubmitting} disabled={!canSubmit(values)} />
              <Button color="secondary" onClick={onClose}>
                {t('common:cancel')}
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

const GuestModal = ({ show, onClose }) => {
  const { t } = useTranslation('landing');
  const [files, setFiles] = useState([]);
  const [terms, showTerms] = useState(false);

  useEffect(
    () => () => {
      if (!show) {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
        setFiles([]);
      }
    },
    [files, show],
  );

  const onDrop = acceptedFiles => {
    const allFiles = [];
    files.forEach(file => allFiles.push(file));

    acceptedFiles.forEach(file =>
      allFiles.push(Object.assign(file, { preview: URL.createObjectURL(file) })),
    );

    setFiles(allFiles);
  };

  const onSubmit = (values, actions) => {
    const params = new FormData();
    actions.setSubmitting(true);

    params.append('email', values.email);
    files.forEach(file => {
      params.append(`identification_documents[]`, file);
    });

    const toastOptions = {
      autoClose: 8000,
      closeButton: false,
    };
    axios
      .post(`${process.env.REACT_APP_API_URL}/new_client_application`, params)
      .then(response => {
        toast.success(response.data.message, toastOptions);
        actions.setSubmitting(false);
        onClose();
      })
      .catch(error => {
        const response = error ? error.response : undefined;
        const data = response ? response.data : undefined;
        const message = data ? data.message : t('common:error');

        toast.error(message, toastOptions);
        actions.setSubmitting(false);
      });
  };

  const initialValues = {
    email: '',
  };
  const canSubmit = values => !!values.email && files.length >= 2;
  const validationSchema = yupObject().shape({
    email: yupString()
      .email()
      .required(),
  });

  const PreviewDropzone = () => {
    return (
      <section className="container">
        <div className="form-group">
          <label className="form-label" style={{ fontSize: '0.9em' }}>
            {t('guestForm.dropzone.title')}
          </label>
          <Dropzone
            labels={{
              waiting: t('guestForm.dropzone.waiting'),
              rejected: t('guestForm.dropzone.rejected'),
            }}
            onDrop={onDrop}
            fileList={files}
          />
        </div>
      </section>
    );
  };

  return (
    <Modal isOpen={show} toggle={onClose}>
      <Formik onSubmit={onSubmit} initialValues={initialValues} validationSchema={validationSchema}>
        {({ isValid, isSubmitting, values, setFieldValue }) => (
          <Form>
            <ModalHeader toggle={onClose}>{t('guestForm.modalTitle')}</ModalHeader>
            <ModalBody>
              <Row>
                <Col xs={12}>
                  <Input
                    type="email"
                    fieldName="email"
                    placeholder={t('guestForm.email.placeholder')}
                    label={t('guestForm.email.label')}
                  />
                </Col>
                <Col xs={12}>
                  <PreviewDropzone />
                </Col>
                <Col xs={12}>
                  <div className="text-center">
                    <Button color="link" className="text-muted" onClick={() => showTerms(!terms)}>
                      {t('privacyPolicy:h1')}
                    </Button>
                  </div>
                  {terms && t('privacyPolicy:applicants').map((line, i) => <p key={i}>{line}</p>)}
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Submit isValid={isValid} isSubmitting={isSubmitting} disabled={!canSubmit(values)} />
              <Button color="secondary" onClick={onClose}>
                {t('common:cancel')}
              </Button>
            </ModalFooter>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

const Content = () => {
  // Here, define the top-level translation functions for use with i18n
  // const { t } = useTranslation('landing');
  const [beauticianModal, setBeauticianModal] = useState(false);
  const [clientModal, setClientModal] = useState(false);

  return (
    <main id="landing-page">
      <CallToAction />

      <OurService />

      <WhyBiyocchi />

      <Price clientModal={clientModal} setClientModal={setClientModal} />

      <Ranking hostModal={beauticianModal} setHostModal={setBeauticianModal} />

      {/* */}
      <HostModal show={beauticianModal} onClose={() => setBeauticianModal(!beauticianModal)} />
      <GuestModal show={clientModal} onClose={() => setClientModal(!clientModal)} />
    </main>
  );
};

export default () => {
  const instaLogo = require('../../assets/images/landing/glyph-logo_May2016.png');
  const [awake, woke] = useState(false);
  const [pinging, ping] = useState(false);

  if (!awake) {
    if (!pinging) {
      axios
        .post(`${process.env.REACT_APP_API_URL}/ping`)
        .then(response => {
          woke(true);
          ping(false);
        })
        .catch(error => {
          toast.error('ネット連携に問題があります。再接続してください。', {
            autoClose: 5000,
            position: toast.POSITION.BOTTOM_CENTER,
          });
          woke(false);
          ping(false);
        });
    }
  }

  return (
    <Default>
      <Header />
      <Content />
      <footer id="landing-footer">
        <a href="https://biyocchi.jp/">BIYOCCHI</a>

        <div className="ml-4">
          <a className="img-link" href="https://www.instagram.com/biyocchi.guest">
            <img src={instaLogo} alt="Instagram" />
          </a>
        </div>
      </footer>
    </Default>
  );
};
