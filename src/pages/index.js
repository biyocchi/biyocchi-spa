import React from 'react';
import { Route, Switch } from 'react-router-dom';

// Components
import Landing from './Landing';
import ConfirmToken from './ConfirmToken';
import Login from './Login';

// Only this constant needs to be updated when adding new routes
const STATIC_ROUTES = [
  { path: '/', component: Landing, opts: { exact: true } },
  { path: '/confirm', component: ConfirmToken, opts: { exact: true } },
  { path: '/login', component: Login, opts: { exact: true } },
];

export default () => (
  <Switch>
    {STATIC_ROUTES.map(({ path, component, opts }, index) => (
      <Route key={index} path={path} component={component} {...opts} />
    ))}
    {/* Catch-all to redirect to login (404) */}
    <Route
      render={({ history, location }) =>
        history.replace({ pathname: '/login', query: { nextPath: location.pathname } })
      }
    />
  </Switch>
);

// In the event we need a render={...} to draw a new component...
// We will have to export an extra constant for it.
// Ex:
// export const RenderedPage = () => <Route path render={Component} />;

// Then we'll have to import with object destructuring.
