import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import { Formik, Form } from 'formik';
import { withRouter } from 'react-router-dom';
import {
  string as yupString,
  object as yupObject,
  ref as matchesField,
  date as yupDate,
  boolean as yupBool,
} from 'yup';
import { Row, Col, Card, CardHeader, CardBody, CardTitle } from 'reactstrap';

import {
  BaseInput as Input,
  BaseSelect as Select,
  BaseCheckbox as Checkbox,
  Submit,
} from '../../shared/FormikInputs';
import Default from '../../layouts/Default';
import Header from '../../shared/Header';
import useAuth from '../../hooks/useAuth';

export default withRouter(
  Object.assign(({ history }) => {
    const [retrievedEmail, setRetrievedEmail] = useState(null);

    useEffect(() => {
      const fetchData = async () => {
        const search = new URLSearchParams(window.location.search);
        try {
          const result = await axios(
            `${process.env.REACT_APP_API_URL}/confirm?token=${search.get('token')}`,
          );
          setRetrievedEmail(result.data.email);
        } catch (error) {
          toast.error(error.response.data.message);
          history.replace('/');
        }
      };
      fetchData();
    }, [history]);

    const currentDate = new Date();
    const minBirthYear = new Date();
    // Min Age 16??
    minBirthYear.setFullYear(currentDate.getFullYear() - 18);

    const { t } = useTranslation('confirmToken');
    const initialValues = {
      username: '',
      password: '',
      password_confirmation: '',
      dob: currentDate.toISOString().slice(0, 10),
      sex: '',
      tos_acceptance: false,
    };
    const validationSchema = yupObject().shape({
      username: yupString().required(),
      password: yupString()
        .min(6, t('form.errors.passwordNotLength'))
        .required(),
      password_confirmation: yupString()
        .min(6)
        .equals([matchesField('password')], t('form.errors.passwordNotMatch'))
        .required(),
      dob: yupDate().max(minBirthYear, t('form.errors.dob')),
      sex: yupString().required(t('form.errors.sex')),
      tos_acceptance: yupBool()
        .equals([true], t('form.errors.tos_acceptance'))
        .required(),
    });
    const { login } = useAuth();

    const onSubmit = (values, actions) => {
      const params = new FormData();
      actions.setSubmitting(true);

      Object.keys(values).forEach(formAttr => {
        params.append(formAttr, values[formAttr]);
      });
      // Not needed server-side
      params.delete('password_confirmation');

      const search = new URLSearchParams(window.location.search);
      params.append('token', search.get('token'));

      axios
        .post(`${process.env.REACT_APP_API_URL}/confirm`, params)
        .then(response => {
          actions.setSubmitting(false);
          login(retrievedEmail, params.get('password'))
            .then(() => {
              window.location.assign('/my_profile');
            })
            .catch(e => {
              actions.setSubmitting(false);
              toast.error('Could not login at this time.');
            });
        })
        .catch(error => {
          let response = error ? error.response : undefined;
          response = !!response ? response.data : undefined;
          const message = !!response ? response.message : t('common:error');

          toast.error(message);
          actions.setSubmitting(false);
        });
    };

    return (
      <Default>
        <Header />

        <section className="container">
          <Row>
            <Col xs={12} md={{ size: 10, offset: 1 }} lg={{ size: 8, offset: 2 }}>
              <Card>
                <CardHeader tag="h3">{t('header')}</CardHeader>
                <CardBody>
                  <CardTitle>{t('title')}</CardTitle>
                  <Formik
                    onSubmit={onSubmit}
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                  >
                    {({ isValid, isSubmitting, values, setFieldValue }) => (
                      <Form>
                        <Input
                          type="username"
                          fieldName="username"
                          placeholder={t('form.username')}
                        />

                        <Input
                          type="password"
                          fieldName="password"
                          placeholder={t('form.password')}
                        />
                        <Input
                          type="password"
                          fieldName="password_confirmation"
                          placeholder={t('form.password_confirmation')}
                        />
                        <Row>
                          <Col xs={6}>
                            <Input type="date" fieldName="dob" placeholder={t('form.dob')} />
                          </Col>
                          <Col xs={6}>
                            <Select fieldName="sex" options={t('form.sex')} />
                          </Col>
                        </Row>
                        <hr />

                        <Checkbox
                          fieldName="tos_acceptance"
                          lowerText={t('form.lowerText.tos_acceptance')}
                          label={t('form.tos_acceptance')}
                        />

                        <br />
                        <Submit isValid={isValid} isSubmitting={isSubmitting} />
                      </Form>
                    )}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </section>
      </Default>
    );
  }),
);
