import React from 'react';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import { Formik, Form } from 'formik';
import { withRouter } from 'react-router-dom';
import { BaseInput as Input, Submit } from '../../shared/FormikInputs';
import Default from '../../layouts/Default';
import Header from '../../shared/Header';
import useAuth from '../../hooks/useAuth';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';

const Login = ({ history, location }) => {
  const { t } = useTranslation();
  const { login } = useAuth();

  const onSubmit = (values, actions) => {
    actions.setSubmitting(true);
    const { email, password } = values;

    login(email, password)
      .then(() => {
        actions.setSubmitting(false);
        const { query } = location;
        const nextPath = query ? query.nextPath : '/my_profile';
        window.location.assign(nextPath);
      })
      .catch(() => {
        actions.setSubmitting(false);
        toast.error('Could not login at this time.');
      });
  };

  return (
    <Default>
      <Header />

      <section className="container">
        <Row>
          <Col xs={12} md={{ size: 10, offset: 1 }} lg={{ size: 8, offset: 2 }}>
            <Card>
              <CardHeader tag="h3">Login</CardHeader>
              <CardBody>
                <Formik
                  onSubmit={onSubmit}
                  initialValues={{ email: '', password: '' }}
                  // validationSchema={validationSchema}
                >
                  {({ isValid, isSubmitting, values, setFieldValue }) => (
                    <Form>
                      <Input type="email" fieldName="email" placeholder={t('email')} />

                      <Input type="password" fieldName="password" placeholder={t('password')} />
                      <hr />

                      <Submit isValid={isValid} isSubmitting={isSubmitting} />
                    </Form>
                  )}
                </Formik>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </section>
    </Default>
  );
};

export default withRouter(Login);
