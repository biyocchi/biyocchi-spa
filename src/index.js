import React from 'react';
import { toast } from 'react-toastify';
import { BrowserRouter as Router } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Routes from './Routes';
import * as serviceWorker from './serviceWorker';

import { AuthProvider } from './hooks/useAuth';
import 'react-toastify/dist/ReactToastify.css';
import './assets/css/main.scss';
import './i18n';
toast.configure();

const Root = () => (
  <AuthProvider>
    <Router>
      <Routes />
    </Router>
  </AuthProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
