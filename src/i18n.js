import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// Helper for populating a JSON with all the translation strings.
// With this, each locale will be populated with namespaces based on the filename
const compileStrings = ctx => {
  const compiled = { resources: {}, ns: [] };
  // This regex does not work for en-US, en-UK, etc.
  // This also relies on camelCased filenames
  const fileRegex = /(?<context>\w*)\/(?<namespace>\w+)\/(?<fileName>\w+)\.(?<locale>\w+)\.json$/;

  ctx.keys().forEach(file => {
    const { fileName, namespace, locale } = file.match(fileRegex).groups;

    compiled.ns.push(namespace);
    compiled.resources[locale] = !!compiled.resources[locale] ? compiled.resources[locale] : {};
    compiled.resources[locale][fileName] = ctx(file);
  });

  // Filter unique namespaces
  compiled.ns = compiled.ns.filter((value, index, self) => {
    return self.indexOf(value) === index;
  });

  return compiled;
};

const strings = compileStrings(require.context('../locale', true, /\.\w+\.json$/));

// import Backend from 'i18next-xhr-backend';
// import LanguageDetector from 'i18next-browser-languagedetector';
// not like to use this?
// have a look at the Quick start guide
// for passing in lng and translations on init
i18n
  // load translation using xhr -> see /public/locales
  // learn more: https://github.com/i18next/i18next-xhr-backend
  // .use(Backend)
  // detect user language
  // learn more: https://github.com/i18next/i18next-browser-languageDetector
  // .use(LanguageDetector)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // init i18next
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    resources: strings.resources,
    ns: strings.ns,
    defaultNS: 'common',
    fallbackLng: 'ja',
    debug: process.env.NODE_ENV === 'development',

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    returnObjects: true,
  });

export default i18n;
