import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

// We can automate at least the Header of the Landing Page items.
export default Object.assign(
  ({ id, children }) => {
    const { t, i18n } = useTranslation('landing');

    return (
      <section id={id}>
        <div className="container">
          {/* Header */}
          <h2>
            {t(`${id}.h2`).map((text, i) => (
              <p key={`${id}-${i}`}>{text}</p>
            ))}
          </h2>

          {/* If intro text exists, format it with needed line breaks*/}
          {i18n.exists(`landing:${id}.intro`) && (
            <div>
              <p>
                {t(`${id}.intro`).map((text, i) => (
                  <Fragment key={`${id}-${i}`}>
                    {text}
                    <br />
                  </Fragment>
                ))}
              </p>
            </div>
          )}

          {children}
        </div>
      </section>
    );
  },
  {
    propTypes: {
      id: PropTypes.string.isRequired,
      children: PropTypes.oneOfType([PropTypes.object.isRequired, PropTypes.array.isRequired]),
    },
  },
);
