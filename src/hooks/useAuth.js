import React, { useState, useContext, createContext } from 'react';
import { get, post, del } from '../utils/queries';

const AuthContext = createContext();

const useAuthState = () => {
  const prevAuth = window.localStorage.getItem('authorized') === 'true';
  const [performed, setPerformed] = useState(false);

  const [authorized, setAuthorized] = useState(prevAuth);
  const [user, setUser] = useState(null);

  // If authorized && no user, retrieve & set user from DB
  if (authorized && !user && !performed) {
    get('/whoami')
      .then(({ data }) => {
        setUser(data);
        setPerformed(true);
      })
      .catch(() => {
        setAuthorized(false);
        window.localStorage.clear();
      });
  }

  const login = async (email, password) => {
    let response = await post('/sign_in', {
      user: { email, password },
    })
      .then(r => {
        window.localStorage.setItem('authorized', true);
        setAuthorized(r.status === 201);
        console.log('success.');
        console.log(r.data);
        setUser(r.data);
        // This will be returned when the promie finally resolves
        return r;
      })
      .catch(e => {
        setUser(null);
        window.localStorage.clear();
        throw e;
      });
    return response;
  };

  const logout = async () => {
    const resp = await del('/sign_out').then(() => {
      setAuthorized(false);
      setUser(null);
    });
    window.localStorage.clear();

    return resp;
  };

  return {
    loading: !performed,
    authorized,
    user,
    login,
    logout,
  };
};

export const AuthProvider = ({ children }) => {
  const value = useAuthState();
  // console.log(value);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default () => {
  return useContext(AuthContext);
};
