import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { get, perform } from '../../utils/queries';

import App from '../../layouts/App';
import Header from '../../shared/Header';

const propTypes = {
  history: PropTypes.object,
};

const MyProfile = ({ history }) => {
  const renderDebug = () => {
    if (process.env.NODE_ENV !== 'development') {
      return null;
    }

    const onClick = e => {
      e.preventDefault();

      const method = document.getElementById('_DB_METHOD').value;
      const endpoint = document.getElementById('_DB_ENDPOINT').value;
      const { value } = document.getElementById('_DB_VALS');
      const data = JSON.parse(value || '{}');
      const params = new FormData();

      Object.entries(data).forEach(item => {
        params.append(item[0], item[1]);
      });

      perform('/'.concat(endpoint), method, params)
        .then(response => {
          console.log('data', response.data);
          console.log('params', response.data.params);
        })
        .catch(e => {
          console.error(e);
          console.log(e.data);
        });
    };

    return (
      <React.Fragment>
        <button onClick={() => get('/whoami').then(r => console.log(r.data))}>whoami</button>

        <form>
          <input id="_DB_METHOD" type="text" />
          <input id="_DB_ENDPOINT" type="text" />
          <input id="_DB_VALS" type="text" />

          <button onClick={onClick}>Perform</button>
        </form>
      </React.Fragment>
    );
  };

  return (
    <App>
      <Header />
      <h1>{history.location.pathname}</h1>

      {renderDebug()}
    </App>
  );
};

MyProfile.propTypes = propTypes;
export default withRouter(MyProfile);
