import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// Components
import MyProfile from './MyProfile';

const AUTH_ROUTES = [{ path: '/my_profile', component: MyProfile, opts: { exact: true } }];

export default () => (
  <Switch>
    {AUTH_ROUTES.map(({ path, component, opts }, index) => (
      <Route key={index} path={path} component={component} {...opts} />
    ))}
    <Route render={() => <Redirect to="/my_profile" />} />
  </Switch>
);

// In the event we need a render={...} to draw a new component...
// We will have to export an extra constant for it.
// Ex:
// export const RenderedPage = () => <Route path render={Component} />;

// Then we'll have to import with object destructuring.
