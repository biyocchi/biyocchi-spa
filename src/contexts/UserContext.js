import React, { useEffect, useState } from 'react';
export const UserContext = React.createContext();

export default ({ children }) => {
  const prevAuth = window.localStorage.getItem('authorized') || false;
  // const prevAuthToken = window.localStorage.getItem('authToken')) || null;

  const [authorized, setAuthorized] = useState(prevAuth);
  // const [authToken, setAuthToken] = useState(prevAuthBody);
  useEffect(
    () => {
      window.localStorage.setItem('authorized', authorized);
      // window.localStorage.setItem('authToken', authToken);
    },
    // [authorized, authToken]
    [authorized],
  );

  const defaultContext = {
    authorized,
    setAuthorized,
    // authToken,
    // setAuthToken
  };

  return <UserContext.Provider value={defaultContext}>{children}</UserContext.Provider>;
};
